import Vue from 'vue'
import dayjs from 'dayjs'
import buddhistEra from 'dayjs/plugin/buddhistEra'
import utc from 'dayjs/plugin/utc'

import 'dayjs/locale/th'
import 'dayjs/locale/en'

dayjs.extend(buddhistEra)
dayjs.extend(utc)

Vue.prototype.$dayjs = dayjs

export default dayjs
