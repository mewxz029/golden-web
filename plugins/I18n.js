import { configure } from 'vee-validate'

export default ({ app }) => {
  configure({
    defaultMessage: (field, values) => app.i18n.t(`validation.${values._rule_}`, values)
  })
}
