export default {
  server: {
    host: '0.0.0.0', // default: localhost
    port: process.env.PORT || 3000
  },
  target: 'static',
  // Global page headers: https://go.nuxtjs.dev/config-head
  head: {
    title: 'AutoPlay',
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { hid: 'description', name: 'description', content: '' },
      { name: 'format-detection', content: 'telephone=no' }
    ],
    link: [
      { rel: 'icon', type: 'image/svg+xml', href: '/svg/pkm-logo-2.svg' }
    ]
  },

  // Global CSS: https://go.nuxtjs.dev/config-css
  css: [
    '@mdi/font/css/materialdesignicons.min.css'
  ],

  // Plugins to run before rendering page: https://go.nuxtjs.dev/config-plugins
  plugins: [
    { src: '@/plugins/VueAwesomeSwiper.js' },
    { src: '@/plugins/I18n.js' },
    { src: '@/plugins/VeeValidate.js' },
    { src: '@/plugins/Dayjs.js' },
    { src: '@/plugins/ClickOutside.js' }
  ],

  // Auto import components: https://go.nuxtjs.dev/config-components
  components: true,

  // Modules for dev and build (recommended): https://go.nuxtjs.dev/config-modules
  buildModules: [
    // https://go.nuxtjs.dev/tailwindcss
    '@nuxtjs/tailwindcss',
    '@nuxtjs/google-fonts',
    '@nuxt/image'
  ],

  // Modules: https://go.nuxtjs.dev/config-modules
  modules: [
    '@nuxt/content',
    '@nuxtjs/i18n',
    'nuxt-route-meta'
  ],

  // Build Configuration: https://go.nuxtjs.dev/config-build
  build: {
    transpile: [
      ({ filePath }) => /(\.esm\.js|\.mjs)$/.test(filePath),
      'vee-validate/dist/rules',
      'vee-validate/dist/locale/th',
      'vee-validate/dist/locale/en'
    ]
  },

  googleFonts: {
    families: {
      Kanit: [300, 400, 500, 700]
    },
    display: 'swap'
  },

  i18n: {
    defaultLocale: 'th',
    locales: [
      {
        code: 'th',
        iso: 'th-TH',
        file: 'th.js',
        name: 'Thailand'
      },
      {
        code: 'en',
        iso: 'en-US',
        file: 'en.js',
        name: 'English'
      }
    ],
    lazy: true,
    langDir: 'locales/',
    vueI18n: {
      fallbackLocale: 'th'
    }
  }
}
