/* eslint-disable global-require */
module.exports = {
  content: [
    './components/**/*.{vue,js}',
    './layouts/**/*.vue',
    './pages/**/*.vue',
    './plugins/**/*.{js,ts}',
    './nuxt.config.{js,ts}'
  ],
  // darkMode: false, // or 'media' or 'class'
  theme: {
    backgroundColor: (theme) => ({
      ...theme('colors'),
      transparent: 'transparent',
      primary: '#8768AE',
      secondary: '#005281',
      third: '#243D5A',
      gray: '#e5e5e5',
      danger: 'rgb(231, 76, 60)',
      gradient: '#8768AE',
      light: '#B191D9'
    }),
    textColor: (theme) => ({
      ...theme('colors'),
      current: 'currentColor',
      primary: '#8768AE',
      secondary: '#005281',
      third: '#243D5A',
      info: '#7A7A7A',
      danger: 'rgb(231, 76, 60)',
      gradient: '#8768AE'
    }),
    borderColor: (theme) => ({
      ...theme('colors'),
      current: 'currentColor',
      primary: '#8768AE',
      secondary: '#005281',
      third: '#243D5A',
      danger: 'rgb(231, 76, 60)',
      gradient: '#8768AE'
    }),
    divideColor: (theme) => ({
      ...theme('colors'),
      current: 'currentColor',
      primary: '#8768AE',
      secondary: '#005281',
      third: '#243D5A',
      gradient: '#8768AE'
    }),
    fontFamily: {
      sans: ['Kanit', 'Arial', 'sans-serif'],
      serif: ['Kanit', 'Arial', 'sans-serif'],
      mono: ['Kanit', 'Arial', 'sans-serif'],
      display: ['Kanit', 'Arial', 'sans-serif'],
      body: ['Kanit', 'Arial', 'sans-serif']
    },
    container: {
      center: true,
      padding: '0.75rem'
    },
    screens: {
      '576': { min: '576px' },
      'xs': { max: '639px' },
      'sm': { min: '640px' },
      'md': { min: '768px' },
      'lg': { min: '1024px' },
      'xl': { min: '1280px' },
      '2xl': { min: '1536px' }
    },
    extend: {}
  },
  variants: {
    extend: {
      fontWeight: ['hover', 'focus']
    }
  },
  plugins: []
}
